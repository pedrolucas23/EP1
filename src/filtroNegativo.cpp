#include "filtroNegativo.hpp"
#include "imagem.hpp"
#include <stdlib.h>
#include <iostream>

Negativo::Negativo(){}

Negativo::Negativo(Imagem imagemComFiltro){
	this->imagemComFiltro = imagemComFiltro;
}

void Negativo::filtragem(){
	struct matrizCores ** pixels;
	int i, j;
	
	pixels = (struct matrizCores **)malloc(imagemComFiltro.getAltura() * sizeof(struct matrizCores *));
  for (i=0; i < imagemComFiltro.getAltura(); i++){
    pixels[i] = (struct matrizCores *)malloc(imagemComFiltro.getLargura() * sizeof(struct matrizCores));
  }

		for(i = 0; i < imagemComFiltro.getAltura(); i++){
    		for(j = 0; j < imagemComFiltro.getLargura(); j++){
        pixels[i][j].r = (unsigned char)255 - imagemComFiltro.getPixel(i,j).r;
        pixels[i][j].g = (unsigned char)255 - imagemComFiltro.getPixel(i,j).g;
        pixels[i][j].b = (unsigned char)255 - imagemComFiltro.getPixel(i,j).b;
    }
  }
imagemComFiltro.setPixels(pixels);
}
