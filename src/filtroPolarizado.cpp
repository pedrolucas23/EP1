#include "filtroPolarizado.hpp"
#include "imagem.hpp"
#include <stdlib.h>
#include <iostream>

Polarizado::Polarizado(){}

Polarizado::Polarizado(Imagem imagemComFiltro){
	this->imagemComFiltro = imagemComFiltro;
}

void Polarizado::filtragem(){
	struct matrizCores ** pixels;
	int i, j;
	
	pixels = (struct matrizCores **)malloc(imagemComFiltro.getAltura() * sizeof(struct matrizCores *));
  for (i=0; i < imagemComFiltro.getAltura(); i++){
    pixels[i] = (struct matrizCores *)malloc(imagemComFiltro.getLargura() * sizeof(struct matrizCores));
  }

		for(i = 0; i < imagemComFiltro.getAltura(); i++){
    		for(j = 0; j < imagemComFiltro.getLargura(); j++){
        if(imagemComFiltro.getPixel(i,j).r < imagemComFiltro.getCores()/2){
             pixels[i][j].r =(unsigned char) 0;
         }else{
             pixels[i][j].r = (unsigned char)imagemComFiltro.getCores();
         }

         if(imagemComFiltro.getPixel(i,j).g < imagemComFiltro.getCores()/2){
             pixels[i][j].g = (unsigned char)0;
         }else{
             pixels[i][j].g = (unsigned char)imagemComFiltro.getCores();
         }

         if(imagemComFiltro.getPixel(i,j).b < imagemComFiltro.getCores()/2){
             pixels[i][j].b = (unsigned char)0;
         }else{
             pixels[i][j].b = (unsigned char)imagemComFiltro.getCores();
         }
    }
  }
imagemComFiltro.setPixels(pixels);
}
