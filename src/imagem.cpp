#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "imagem.hpp"

Imagem::Imagem() {}

Imagem::~Imagem(){} 

void Imagem::setTipoDeImagem(ifstream &file){
   char caracter;
   file >> caracter;
   while (caracter == '#'){
   	file.ignore(1000, '\n');
   }
   file.unget();
   
   file >> tipoDeImagem;
}

string Imagem::getTipoDeImagem() {
   return tipoDeImagem;
}

void Imagem::setLargura(ifstream &file){
   char caracter;
   file >> caracter;
   while (caracter == '#'){
   	file.ignore(1000, '\n');
   	file >> caracter;
   }
   file.unget();
   
   file >> largura;
}

int Imagem::getLargura(){
   return largura;
}

void Imagem::setAltura(ifstream &file){ 
   char caracter;
   file >> caracter;
   while (caracter == '#'){
   	file.ignore(1000, '\n');
   	file >> caracter;
   }
   file.unget();
   
   file >> altura;
}

int Imagem::getAltura(){
   return altura;
}

void Imagem::setCores(ifstream &file){ 
   char caracter;
   file >> caracter;
   while (caracter == '#'){
   	file.ignore(1000, '\n');
   }
   file.unget();
   
   file >> cores;
   file.get();
}

int Imagem::getCores(){
   return cores;
}

void Imagem::tamanhoPixels(int largura, int altura){
  int i;
  pixel = (struct matrizCores **)malloc(altura * sizeof(struct matrizCores *));
  for (i=0; i < altura; i++){
    pixel[i] = (struct matrizCores *)malloc(largura * sizeof(struct matrizCores));
  }
}

void Imagem::setPixels(ifstream &file){
  int i,j;
  for(i=0; i<altura; i++){
    for(j=0; j<largura; j++){
      pixel[i][j].r = file.get();
      pixel[i][j].g = file.get();
      pixel[i][j].b = file.get();
	}
  }
}

void Imagem::setPixels(struct matrizCores ** pixel){
  this->pixel = pixel;
}

struct matrizCores Imagem::getPixel(int altura, int largura){
  return pixel[altura][largura];
}

void Imagem::lerImagem(ifstream &file){
  setTipoDeImagem(file);
  setLargura(file);
  setAltura(file);
  setCores(file);
  tamanhoPixels(getLargura(), getAltura());
  setPixels(file);
}

void Imagem::saveFile(ofstream &newFile){
  newFile << tipoDeImagem << endl;
  newFile << largura <<" "<< altura << endl;
  newFile << cores << endl ;

  int i,j;
  for(i=0; i<altura; i++){
    for(j=0; j<largura; j++)
    newFile << pixel[i][j].r << pixel[i][j].g << pixel[i][j].b;
  }
}


