/*	Aluno: Pedro Lucas Pinheiro de Souza
**	Matrícula: 13/0035581
**
**	Exercício EP1 - 06/09/2016
*/

#include<iostream>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include"imagem.hpp"
#include"filtroNegativo.hpp"
#include"filtroPolarizado.hpp"
#include"filtroPretoEBranco.hpp"
#include"filtro.hpp"

using namespace std;

int main()
{
	Negativo negativo;
	Polarizado polarizado;
	PretoEBranco pretoEBranco;
	Imagem imagem;
	string novoNome, nomeArquivo;
	ifstream arquivoEntrada;
	ofstream arquivoSaida;
	int opcao;
	
	cout << "Digite o nome do arquivo a ser aberto: ";
	cin >> nomeArquivo;
	
	nomeArquivo = "./doc/" + nomeArquivo + ".ppm";
	
	arquivoEntrada.open(nomeArquivo.c_str(), ifstream::binary);
	
	if(!arquivoEntrada)
	{
		cerr << "Erro ao abrir o arquivo!!" << endl;
		return -1;
	}
	
	imagem.lerImagem(arquivoEntrada);
	
	if(imagem.getTipoDeImagem() != "P6"){
		cerr << "Formato incorreto!!!" << endl;
		arquivoEntrada.close();
		return -1;
	}
	
	arquivoEntrada.close();
	
	cout << "Escolha um dos filtros:\n1 - Negativo\n2 - Polarizado\n3 - Preto e Branco\nOpcção:  ";
	cin >> opcao;
	
	switch(opcao){
		case 1:
			negativo.setImagem(imagem);
			negativo.filtragem();
			imagem = negativo.getImagem();
			break;
			
		case 2:
			polarizado.setImagem(imagem);
			polarizado.filtragem();
			imagem = polarizado.getImagem();
			break;
			
		case 3:
			pretoEBranco.setImagem(imagem);
			pretoEBranco.filtragem();
			imagem = pretoEBranco.getImagem();
			break;
	}
	
	
	cout << "Digite o nome do novo arquivo a ser gerado: ";
	cin >> novoNome;
	
	novoNome = novoNome + ".ppm";
	
	arquivoSaida.open(novoNome.c_str());
	imagem.saveFile(arquivoSaida);
	arquivoSaida.close();
	
	cout << "Concluído!!!" << endl;
	
	return 0;
}
