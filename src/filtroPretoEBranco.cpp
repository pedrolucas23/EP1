#include "filtroPretoEBranco.hpp"
#include "imagem.hpp"
#include <stdlib.h>
#include <iostream>

PretoEBranco::PretoEBranco(){}

PretoEBranco::PretoEBranco(Imagem imagemComFiltro){
	this->imagemComFiltro = imagemComFiltro;
}

void PretoEBranco::filtragem(){
	struct matrizCores ** pixels;
	int i, j, escalaCinza;
	
	pixels = (struct matrizCores **)malloc(imagemComFiltro.getAltura() * sizeof(struct matrizCores *));
  for (i=0; i < imagemComFiltro.getAltura(); i++){
    pixels[i] = (struct matrizCores *)malloc(imagemComFiltro.getLargura() * sizeof(struct matrizCores));
  }

		for(i = 0; i < imagemComFiltro.getAltura(); i++){
    		for(j = 0; j < imagemComFiltro.getLargura(); j++){
    		
    		escalaCinza = (0.299 * imagemComFiltro.getPixel(i,j).r) + (0.587 * imagemComFiltro.getPixel(i,j).g) + (0.144 * imagemComFiltro.getPixel(i,j).b);
    		
        pixels[i][j].r = escalaCinza;
        pixels[i][j].g = escalaCinza;
        pixels[i][j].b = escalaCinza;
    }
  }
imagemComFiltro.setPixels(pixels);
}
