#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "imagem.hpp"

using namespace std;

class Filtro{
protected:
  Imagem imagemComFiltro;
  
public:
  Filtro();
  Filtro(Imagem imagemComFiltro);
  //~Filtro();
  void setImagem(Imagem imagemComFiltro);
  Imagem getImagem();
  void filtragem();
};

#endif
