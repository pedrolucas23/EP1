#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"


class Polarizado : public Filtro{
public:
	Polarizado();
	Polarizado(Imagem imagemComFiltro);
	void filtragem();	
};
#endif
