#ifndef FILTROPRETOEBRANCO_HPP
#define FILTROPRETOEBRANCO_HPP

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"


class PretoEBranco : public Filtro{
public:
	PretoEBranco();
	PretoEBranco(Imagem imagemComFiltro);
	void filtragem();	
};
#endif
