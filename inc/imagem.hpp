#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include<iostream>
#include<string>
#include<fstream>

using namespace std;

struct matrizCores{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

class Imagem{
private:
    string tipoDeImagem;
    int largura, altura, cores;
    struct matrizCores ** pixel;
    
public:
    Imagem(); 
	~Imagem();
	
	string getTipoDeImagem();
	void setTipoDeImagem(ifstream &file);
	int getLargura();
	void setLargura(ifstream &file);
	int getAltura();
	void setAltura(ifstream &file);
	int getCores();
	void tamanhoPixels(int largura, int altura);
	void setCores(ifstream &file);
	void setPixels(ifstream &file);
  	void setPixels(struct matrizCores **pixel);
  	struct matrizCores getPixel(int altura, int largura);
  	void lerImagem(ifstream &file);
  	void saveFile(ofstream &newFile);
		
};

#endif
