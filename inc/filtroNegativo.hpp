#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"


class Negativo : public Filtro{
public:
	Negativo();
	Negativo(Imagem imagemComFiltro);
	void filtragem();	
};
#endif
