#    Orientação a Objetos 2/2016 (UnB - Gama)

##   EP1

###  Aluno: Pedro Lucas Pinheiro de Souza
###  Matrícula: 13/0035581

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

#### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
* Digite o nome de uma das imagens que devem estar na pasat .doc do projeto;

* Escolha um dos filtros escritos no programa;

* Digite um novo nome para a imagem já com o filtro aplicado;

* A nova imagem será salva na pasta principal do programa;

